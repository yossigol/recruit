<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('candidates', 'CandidatesController');

Route::get('/hello',function(){
    return 'hello Larevel';
});

Route::get('/student/{id}',function($id = 'no student found'){
    return 'we got student with id'.$id;
});

Route::get('/car/{id?}',function($id = null){
    if(isset($id)){
        return "we got car with id $id";
    }
    else{
        return 'we need the id to find your car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});

//targil 5
Route::get('/users/{email}/{name?}', function ($email,$name = "name missing") {
    return view('targil5',compact('email','name'));
});

